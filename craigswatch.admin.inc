<?php

/**
 * @file
 *   Admin page callbacks for the craigswatch module.
 * @ingroup craigswatch
 */

/**
 * List admin view for locations.
 */
function craigswatch_admin_locations() {
  $header = array(t('Country'), t('State'), t('Cities'));
  $rows = array();
  $country = '';
  $state = '';
  $cities = '';
  $sql = "SELECT * FROM {craigswatch_location} ORDER BY id";
  $result = db_query(db_rewrite_sql($sql));
  while ($data = db_fetch_object($result)) {
    if (strcmp($country, $data->country)) {
      if (strlen($country)) $rows[] = array($country, $state, $cities);
      $country = $data->country;
      $state = $data->state;
      $cities = '';
    }
    else if (strcmp($state, $data->state)) {
      if (strlen($state)) $rows[] = array($country, $state, $cities);
      $state = $data->state;
      $cities = '';
    }
    $cities .= ' '.l($data->city, 'admin/craigswatch/locations/edit/'.$data->id);
  }
  $rows[] = array($country, $state, $cities);
  return theme('table', $header, $rows);
}

/**
 * Implementation of hook_form for location.
 *
 * @ingroup forms
 * @see craigswatch_admin_location_form_validate()
 * @see craigswatch_admin_location_form_submit()
 */
function craigswatch_admin_location_form(&$form_state, $id = NULL) {
  if ($id) {
    $sql = sprintf("SELECT * FROM {craigswatch_location} WHERE id = %d", $id);
    $result = db_query(db_rewrite_sql($sql));
    $data = db_fetch_object($result);
  }
  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => $id,
  );
  $form['country'] = array(
    '#type' => 'textfield',
    '#title' => t('Country'),
    '#default_value' => $data->country,
    '#description' => t('The country for this location. This must be unique.'),
    '#required' => TRUE,
  );
  $form['state'] = array(
    '#type' => 'textfield',
    '#title' => t('State'),
    '#default_value' => $data->state,
    '#description' => t('The state for this location. This must be unique for the country.'),
    '#required' => TRUE,
  );
  $form['city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#default_value' => $data->city,
    '#description' => t('The city for this location. This must be unique for the state.'),
    '#required' => TRUE,
  );
  $form['prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Prefix'),
    '#default_value' => $data->prefix,
    '#description' => t('The prefix for this location. This must be unique.'),
    '#required' => TRUE,
  );
  $form['suffix'] = array(
    '#type' => 'textfield',
    '#title' => t('Suffix'),
    '#default_value' => $data->suffix,
    '#description' => t('The (optional) suffix for this location.'),
  );
  if ($id) {
    $form['update'] = array(
      '#type' => 'submit',
      '#value' => t('Update location'),
      '#weight' => 98,
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete location'),
      '#weight' => 99,
    );
  }
  else {
    $form['add'] = array(
      '#type' => 'submit',
      '#value' => t('Add location'),
      '#weight' => 97,
    );
  }
  return $form;
}

/**
 * Implementation of hook_form_validate for location.
 */
function craigswatch_admin_location_form_validate($form, &$form_state) {
  $country = trim($form_state['values']['country']);
  $state = trim($form_state['values']['state']);
  $city = trim($form_state['values']['city']);
  $prefix = trim($form_state['values']['prefix']);
  $suffix = trim($form_state['values']['suffix']);
  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';
  if ($op == 'Delete location') {
    $sql = sprintf("DELETE FROM {craigswatch_location} WHERE `country` = '%s' AND `state` = '%s' AND `city` = '%s'", $country, $state, $city);
    $result = db_query(db_rewrite_sql($sql));
    if ($result) {
      drupal_set_message('Your location entry has been deleted.');
    }
    drupal_goto('admin/craigswatch/locations');
  }
  else if ($op == 'Add location') {
    $sql = "SELECT * FROM {craigswatch_location}";
    $result = db_query(db_rewrite_sql($sql));
    while ($data = db_fetch_object($result)) {
      if (!strcmp($country, $data->country)) {
        if (!strcmp($state, $data->state)) {
          if (!strcmp($city, $data->city)) {
            form_set_error('location', t('That location already exists in the database.'));
          }
        }
      }
    }
  }
  if (!preg_match('!^[A-Za-z0-9 \-/,.\[\]`]+$!', $country)) {
    form_set_error('country', t('The country must contain only letters, numbers, spaces, dashes, slashes, commas, dots, square brackets, and backticks.'));
  }
  if (!preg_match('!^[A-Za-z0-9 \-/,.\[\]`]+$!', $state)) {
    form_set_error('state', t('The state must contain only letters, numbers, spaces, dashes, slashes, commas, dots, square brackets, and backticks.'));
  }
  if (!preg_match('!^[A-Za-z0-9 \-/,.\[\]`]+$!', $city)) {
    form_set_error('city', t('The city must contain only letters, numbers, spaces, dashes, slashes, commas, dots, square brackets, and backticks.'));
  }
  if (!preg_match('!^[A-Za-z0-9 \-/,.\[\]`]+$!', $prefix)) {
    form_set_error('prefix', t('The prefix must contain only letters, numbers, spaces, dashes, slashes, commas, dots, square brackets, and backticks.'));
  }
  if (!preg_match('!^[A-Za-z0-9 \-/,.\[\]`]+$!', $suffix)) {
    form_set_error('suffix', t('The suffix must contain only letters, numbers, spaces, dashes, slashes, commas, dots, square brackets, and backticks.'));
  }
}

/**
 * Implementation of hook_form_submit for location.
 */
function craigswatch_admin_location_form_submit($form, &$form_state) {
  $country = trim($form_state['values']['country']);
  $state = trim($form_state['values']['state']);
  $city = trim($form_state['values']['city']);
  $prefix = trim($form_state['values']['prefix']);
  $suffix = trim($form_state['values']['suffix']);
  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';
  if ($op == 'Add location') {
    $sql = sprintf("INSERT INTO {craigswatch_location} (`country`,`state`,`city`,`prefix`,`suffix`) VALUES ('%s','%s','%s','%s','%s')", $country,$state,$city,$prefix,$suffix);
    $result = db_query(db_rewrite_sql($sql));
    if ($result) {
      drupal_set_message('Your location entry has been added.');
    }
  }
  else if ($op == 'Update location') {
    $id = db_result(db_query("SELECT id FROM {craigswatch_location} WHERE `country` = '%s' AND `state` = '%s' AND `city` = '%s'", $country, $state, $city));
    $sql = sprintf("UPDATE {craigswatch_location} SET `country` = '%s', `state` = '%s', `city` = '%s', `prefix` = '%s', `suffix` = '%s' WHERE id = %d", $country, $state, $city, $prefix, $suffix, $id);
    $result = db_query(db_rewrite_sql($sql));
    if ($result) {
      drupal_set_message('Your location entry has been updated.');
    }
  }
  drupal_goto('admin/craigswatch/locations');
}

/**
 * List admin view for categories.
 *
 * @return
 *   a themed page of categories.
 */
function craigswatch_admin_categories() {
  $header = array(t('Heading'), t('Subheadings'));
  $rows = array();
  $heading = '';
  $subheadings = '';
  $sql = "SELECT * FROM {craigswatch_category} ORDER BY id";
  $result = db_query(db_rewrite_sql($sql));
  while ($data = db_fetch_object($result)) {
    if (strcmp($heading, $data->heading)) {
      if (strlen($heading)) $rows[] = array($heading, $subheadings);
      $heading = $data->heading;
      $subheadings = '';
    }
    $subheadings .= ' '.l($data->subheading, 'admin/craigswatch/categories/edit/'.$data->id);
  }
  $rows[] = array($heading, $subheadings);
  return theme('table', $header, $rows);
}

/**
 * Implementation of hook_form for category.
 *
 * @ingroup forms
 * @see craigswatch_admin_category_form_validate()
 * @see craigswatch_admin_category_form_submit()
 */
function craigswatch_admin_category_form(&$form_state, $id = NULL) {
  if ($id) {
    $sql = sprintf("SELECT * FROM {craigswatch_category} WHERE id = %d", $id);
    $result = db_query(db_rewrite_sql($sql));
    $data = db_fetch_object($result);
  }
  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => $id,
  );
  $form['heading'] = array(
    '#type' => 'textfield',
    '#title' => t('Heading'),
    '#default_value' => $data->heading,
    '#description' => t('The heading for this category. This must be unique.'),
    '#required' => TRUE,
  );
  $form['subheading'] = array(
    '#type' => 'textfield',
    '#title' => t('Subheading'),
    '#default_value' => $data->subheading,
    '#description' => t('The subheading for this category. This must be unique for the heading.'),
    '#required' => TRUE,
  );
  $form['suffix'] = array(
    '#type' => 'textfield',
    '#title' => t('Suffix'),
    '#default_value' => $data->suffix,
    '#description' => t('The suffix for this category.'),
    '#required' => TRUE,
  );
  if ($id) {
    $form['update'] = array(
      '#type' => 'submit',
      '#value' => t('Update category'),
      '#weight' => 45,
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete category'),
      '#weight' => 50,
    );
  }
  else {
    $form['add'] = array(
      '#type' => 'submit',
      '#value' => t('Add category'),
      '#weight' => 40,
    );
  }
  return $form;
}

/**
 * Implementation of hook_form_validate for category.
 */
function craigswatch_admin_category_form_validate($form, &$form_state) {
  $heading = trim($form_state['values']['heading']);
  $subheading = trim($form_state['values']['subheading']);
  $suffix = trim($form_state['values']['suffix']);
  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';
  if ($op == 'Delete category') {
    $sql = sprintf("DELETE FROM {craigswatch_category} WHERE `heading` = '%s' AND `subheading` = '%s'", $heading, $subheading);
    $result = db_query(db_rewrite_sql($sql));
    if ($result) {
      drupal_set_message('Your category entry has been deleted.');
    }
    drupal_goto('admin/craigswatch/categories');
  }
  else if ($op == 'Add category') {
    $sql = "SELECT * FROM {craigswatch_category}";
    $result = db_query(db_rewrite_sql($sql));
    while ($data = db_fetch_object($result)) {
      if (!strcmp($heading, $data->heading)) {
        if (!strcmp($subheading, $data->subheading)) {
          form_set_error('category', t('That category already exists in the database.'));
        }
      }
    }
  }
  if (!preg_match('!^[A-Za-z0-9 \-/,.+`]+$!', $heading)) {
    form_set_error('heading', t('The heading must contain only letters, numbers, spaces, dashes, slashes, commas, dots, plus signs, and backticks.'));
  }
  if (!preg_match('!^[A-Za-z0-9 \-/,.+`]+$!', $subheading)) {
    form_set_error('subheading', t('The subheading must contain only letters, numbers, spaces, dashes, slashes, commas, dots, plus signs, and backticks.'));
  }
  if (!preg_match('!^[A-Za-z0-9 \-/,.+`]+$!', $suffix)) {
    form_set_error('suffix', t('The suffix must contain only letters, numbers, spaces, dashes, slashes, commas, dots, plus signs, and backticks.'));
  }
}

/**
 * Implementation of hook_form_submit for category.
 */
function craigswatch_admin_category_form_submit($form, &$form_state) {
  $heading = trim($form_state['values']['heading']);
  $subheading = trim($form_state['values']['subheading']);
  $suffix = trim($form_state['values']['suffix']);
  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';
  if ($op == 'Add category') {
    $sql = sprintf("INSERT INTO {craigswatch_category} (`heading`,`subheading`,`suffix`) VALUES ('%s','%s','%s')", $heading,$subheading,$suffix);
    $result = db_query(db_rewrite_sql($sql));
    if ($result) {
      drupal_set_message('Your category entry has been added.');
    }
  }
  else if ($op == 'Update category') {
    $id = db_result(db_query("SELECT id FROM {craigswatch_category} WHERE `heading` = '%s' AND `subheading` = '%s'", $heading, $subheading));
    $sql = sprintf("UPDATE {craigswatch_category} SET `heading` = '%s', `subheading` = '%s', `suffix` = '%s' WHERE id = %d", $heading, $subheading, $suffix, $id);
    $result = db_query(db_rewrite_sql($sql));
    if ($result) {
      drupal_set_message('Your category entry has been updated.');
    }
  }
  drupal_goto('admin/craigswatch/categories');
}

