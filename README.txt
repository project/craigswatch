
README.txt file for the craigswatch module

SETTING UP YOUR BLUEHOST SITE
-----------------------------

1 - From your bluehost cpanel front page, select "Mail" > "Email Accounts".
2 - Follow the directions to create an email address.

CREATING A DRUPAL SITE
----------------------

1 - From your bluehost cpanel front page, run "Software / Services" > "Fantastico De Luxe" (near the bottom of the page).
2 - From the menu on the left, run "Content Management" > "Drupal".
3 - From the "Drupal" box, click "New Installation".
4 - Enter an installation directory name (something like "drupal_6_6").
5 - Enter your "Admin access data" (something like "admin" for the admin username, and something you'll remember for a password).
6 - Enter your "Base configuration" email address (the email address you created above).
7 - Click the "Install drupal" button.
8 - Upon completion, click the "Finish installation" button.
9 - Send yourself an email (using your normal email address) to have a record of the database and user names created, as well as the domain, subdirectory, and access url.

INSTALLING THE CRAIGSWATCH MODULE
---------------------------------

The craigswatch module installation process is the same as any other standard drupal module installation.

1 - From your bluehost cpanel front page, run "Files" > "File Manager".
2 - From the File Manager page, open the "drupal_6_6" directory (if that's what you called it).
3 - Open the "sites" directory, and then the "all" directory.
4 - Create a "modules" directory (folder) under the "sites/all" directory.
5 - Create a "craigswatch" directory under the "sites/all/modules" directory.
6 - Upload your craigswatch files to that directory.
7 - On your drupal site, run "Administer" > "Modules".
8 - Enable the "Craigswatch" module (near the bottom of the page).
9 - Run "Administer" > "Blocks", and move the "Craigswatch" block to the "Left sidebar".

CHECKING THE CRAIGSWATCH MODULE INSTALLATION
--------------------------------------------

1 - Run "Administer".
2 - You should see a "Craigswatch" administration section with "Locations" and "Categories" links.
3 - From the "Craigswatch" block (back to the front page again), click the "Watches" link.
4 - Click "Watches" > "Add" to add a watch.
5 - Select your location, then select "for sale cars+trucks" as your category.
6 - Enter "Honda" as your first search term, then click the "Add watch" button.
7 - Wait for at least an hour.
8 - Assuming someone is selling a Honda on your local craigslist, you should receive an email for each one found.
9 - NOTE: The craigswatch module only checks the first 100 entries (the first page) for any particular item.

ABOUT THE CRAIGSWATCH CRON JOB
------------------------------

The cron job ("hook_cron" function) for the craigswatch module uses the "file_get_contents" function. This means that if you're running your site on a shared server, you must have applied for (and received) shell access. If you do not yet have a site (or if you're not sure you can get shell access) you can sign up for a bluehost account here: http://www.bluehost.com/track/carlwenrich/

I have only tested the craigswatch module on a bluehost server, so that is the only one about which I can answer questions if you have problems. And it is the only one that I am sure that you will receive all the tools (for example cpanel) that you will need to run the drupal site with the craigswatch module.

Once you have a bluehost account, you can apply for shell access by faxing them a copy of your driver's licence, along with a note requesting shell access. Unless you're a convicted felon, you should have it with a day or two.

You must set up the cron to run the "<your home directory>/www/<your drupal installation directory>/cron.php" script unless you intend to run the cron manually (which sort of defeats the purpose). Near the bottom of the cpanel front page run "Advanced" > "Cron jobs" and follow the directions to create a cron job. Make a note of your "Home Directory" (left section of your cpanel front page) and then enter "php -q <your home directory>/www/<your drupal installation directory>/cron.php" as the "Command to run" when filling out the form. Bluehost will change the "php" to something like "/ramdisk/bin/php5".

