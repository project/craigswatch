<?php

/**
 * @file
 *   Main file for the craigswatch module.
 *
 *   A user selects a location, a category, and from zero to three search terms to add a watch.
 *   A cron job running every hour pulls relevent listings from craigslist and sends an email to the user for each one.
 *   The user can list (and optionally remove) his watches and his listings.
 * @defgroup craigswatch
 * @ingroup craigswatch
 */

/**
 * Implementation of hook_perm.
 */
function craigswatch_perm() {
  return array('access craigswatch');
}

/**
 * Parse the craigslist line and update the listing database table.
 *
 * @param $s
 *   a string containing a line from the craigslist page being parsed.
 * @param $data
 *   a watch record.
 * @param $ldata
 *   a location record.
 */
function craigswatch_update_db($s, $data, $ldata) {
  if (strstr($s, 'href=')) {
    $a1 = explode('href="', $s); // a1[0] = "<p><a "
    $a2 = explode('"', $a1[1]); // a2[1] = link to listing without "http://".[location prefix]."craigslist".[location extension] part
    $a3 = explode('>', $a2[1]);
    $a4 = explode('-<', $a3[1]);
    $body = 'http://'. $ldata->prefix .'.craigslist.'. $ldata->extension . $a2[0]; // a2[0] = [category suffix]
    $listing = addslashes($a1[0] .'href="'. $body .'"'. $a2[1]);
    $listing = str_replace('/', '|', $listing);
    if(strpos($listing, '(?)')) return;
    if(strpos($listing, 'RSS')) return;
    if(strpos($listing, 'help')) return;
    if(strpos($listing, 'stylesheet')) return;
    if(strpos($listing, 'craigslist<')) return;
    if(strpos($listing, 'PERSONAL SAFETY TIPS')) return;
    if(strpos($listing, 'Back to top of page')) return;
    if(strpos($listing, 'add.my.yahoo')) return;
    if(strpos($listing, 'med/index.rss')) return;
    if(strpos($listing, 'forumID')) return;
    if(strpos($listing, 'post.craigslist')) return;
    if(strpos($listing, 'about/rss')) return;
    if(strpos($listing, 'about/help')) return;
    if(strpos($listing, 'index100')) return;
    if(strpos($listing, 'prohibited.items')) return;
    if(strpos($listing, 'index.rss')) return;
    $msql = sprintf("SELECT * FROM {users} WHERE `uid` = %d", $data->uid);
    $mresult = db_query($msql);
    $mdata = db_fetch_object($mresult);
    $csql = sprintf("SELECT count(*) as count FROM {craigswatch_listing} WHERE `uid` = %d AND `listing` = '%s'", $mdata->uid, $listing);
    $cresult = db_query($csql);
    $cdata = db_fetch_object($cresult);
    if ($cdata->count == 0) {
      if ($data->send_email) {
        $subject = $a4[0]; // a4[0] = listing title with price
        $to = $mdata->mail;
        if (!mail($to, $subject, $body)) echo('email failed: '. $to .'<br>');
      }
      $sql = sprintf("INSERT INTO {craigswatch_listing} (`uid`,`timestamp`,`listing`) VALUES (%d,%d,'%s')", $mdata->uid, time(), $listing);
      $result = db_query($sql);
    }
  }
}

/**
 * Implementation of hook_cron.
 *
 * Scan a page of 100 craigslist postings, parse each line, create listing records for entries that match user criteria.
 */
function craigswatch_cron() {
  global $user;
  $sql = "SELECT * FROM {craigswatch_watch}";
  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {
    $lsql = sprintf("SELECT * FROM {craigswatch_location} WHERE id = %d", $data->lid);
    $lresult = db_query($lsql);
    $ldata = db_fetch_object($lresult);
    $csql = sprintf("SELECT * FROM {craigswatch_category} WHERE id = %d", $data->cid);
    $cresult = db_query($csql);
    $cdata = db_fetch_object($cresult);
    $msg = sprintf("%s %s %s %s %s %s %s %s", $ldata->country, $ldata->state, $ldata->city, $cdata->heading, $cdata->subheading, $data->term_1, $data->term_2, $data->term_3);
    $path = str_replace('|', '/', 'http://'. $ldata->prefix .'.craigslist.'. $ldata->extension .'/'. $ldata->suffix . $cdata->suffix);
    $lines = file_get_contents($path);
    $a = explode("\n", $lines);
    foreach ($a as $s) {
      if (strlen($data->term_1) > 0) {
        if (stristr($s, $data->term_1)) {
          if (strlen($data->term_2) > 0) {
            if (stristr($s, $data->term_2)) {
              if (strlen($data->term_3) > 0) {
                if (stristr($s, $data->term_3)) {
                  craigswatch_update_db($s, $data, $ldata);
                }
              }
              else {
                craigswatch_update_db($s, $data, $ldata);
              }
            }
          }
          else {
            craigswatch_update_db($s, $data, $ldata);
          }
        }
      }
      else {
        craigswatch_update_db($s, $data, $ldata);
      }
    }
  }
}

/**
 * Implementation of hook_block.
 */
function craigswatch_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks[0]['info'] = t('Craigswatch');
      return $blocks;
    case 'view':
      $items = array(
        l(t('Watches'), 'craigswatch/watches'),
        l(t('Listings'), 'craigswatch/listings'),
        l(t('Tutor'), 'craigswatch/tutor'),
      );
      $block['subject'] = t('Craigswatch');
      $block['content'] = theme('item_list', $items);
      return $block;
  }
}

/**
 * Implementation of hook_help.
 */
function craigswatch_help($path, $arg) {
  global $base_url;

  switch ($path) {
    case 'admin/craigswatch/locations':
      return '<p>'. t("Here you can find all the locations served by this module. Click on a [city] link to edit it's parameters (or to delete it).") .'</p>';
      break;
    case 'admin/craigswatch/categories':
      return '<p>'. t("Here you can find all the categories served by this module. Click on a [subheading] link to edit it's parameters (or to delete it).") .'</p>';
      break;
    case 'craigswatch/watches':
      return '<p>'. t("Here you can find all the watches that you have entered. Click on an [edit] link to edit the watch's parameters (or to delete it).") .'</p>';
      break;
    case 'craigswatch/listings':
      return '<p>'. t("Here you can find all the listings that have been generated by your watches. Click on the [Delete all] link to delete them.") .'</p>';
      break;
    case 'craigswatch/tutor':
      return '<p>'. t("NOTE: You must be registered and logged in to use the craigswatch functions.") .'</p>';
      break;
  }
}

/**
 * Implementation of hook_menu.
 */
function craigswatch_menu() {
  $items['admin/craigswatch'] = array(
    'title' => t('Craigswatch'),
    'description' => t('Administer craigswatch locations and categories.'),
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer site configuration'),
    'position' => 'left',
    'weight' => -99,
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );

  $items['admin/craigswatch/locations'] = array(
    'title' => t('Locations'),
    'description' => t('Administer craigswatch locations'),
    'page callback' => 'craigswatch_admin_locations',
    'access arguments' => array('administer site configuration'),
    'weight' => -99,
    'file' => 'craigswatch.admin.inc',
    'file path' => drupal_get_path('module', 'craigswatch'),
  );
  $items['admin/craigswatch/locations/list'] = array(
    'title' => t('List'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -99,
    'file' => 'craigswatch.admin.inc',
    'file path' => drupal_get_path('module', 'craigswatch'),
  );
  $items['admin/craigswatch/locations/add'] = array(
    'title' => t('Add'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('craigswatch_admin_location_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_TASK,
    'weight' => -98,
    'file' => 'craigswatch.admin.inc',
    'file path' => drupal_get_path('module', 'craigswatch'),
  );
  $items['admin/craigswatch/locations/edit'] = array(
    'title' => t('Edit'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('craigswatch_admin_location_form', 4),
    'access arguments' => array('administer site configuration'),
    'file' => 'craigswatch.admin.inc',
    'file path' => drupal_get_path('module', 'craigswatch'),
  );

  $items['admin/craigswatch/categories'] = array(
    'title' => t('Categories'),
    'description' => t('Administer craigswatch categories'),
    'page callback' => 'craigswatch_admin_categories',
    'access arguments' => array('administer site configuration'),
    'weight' => -98,
    'file' => 'craigswatch.admin.inc',
    'file path' => drupal_get_path('module', 'craigswatch'),
  );
  $items['admin/craigswatch/categories/list'] = array(
    'title' => t('List'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -99,
    'file' => 'craigswatch.admin.inc',
    'file path' => drupal_get_path('module', 'craigswatch'),
  );
  $items['admin/craigswatch/categories/add'] = array(
    'title' => t('Add'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('craigswatch_admin_category_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_TASK,
    'weight' => -98,
    'file' => 'craigswatch.admin.inc',
    'file path' => drupal_get_path('module', 'craigswatch'),
  );
  $items['admin/craigswatch/categories/edit'] = array(
    'title' => t('Edit'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('craigswatch_admin_category_form', 4),
    'access arguments' => array('administer site configuration'),
    'file' => 'craigswatch.admin.inc',
    'file path' => drupal_get_path('module', 'craigswatch'),
  );

  $items['craigswatch/watches'] = array(
    'title' => t('Watches'),
    'description' => t('Craigswatch watches'),
    'page callback' => 'craigswatch_watches',
    'access arguments' => array('access craigswatch'),
    'type' => MENU_CALLBACK,
    'weight' => -99,
  );
  $items['craigswatch/watches/list'] = array(
    'title' => t('List'),
    'access arguments' => array('access craigswatch'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -99,
  );
  $items['craigswatch/watches/add'] = array(
    'title' => t('Add'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('craigswatch_watch_form'),
    'access arguments' => array('access craigswatch'),
    'type' => MENU_LOCAL_TASK,
    'weight' => -98,
  );
  $items['craigswatch/watches/edit'] = array(
    'title' => t('Edit'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('craigswatch_watch_form', 3, 4, 5),
    'access arguments' => array('access craigswatch'),
    'type' => MENU_CALLBACK,
    'weight' => -97,
  );

  $items['craigswatch/listings'] = array(
    'title' => t('Listings'),
    'description' => t('Craigswatch listings'),
    'page callback' => 'craigswatch_listings',
    'access arguments' => array('access craigswatch'),
    'type' => MENU_CALLBACK,
    'weight' => -99,
  );
  $items['craigswatch/listings/list'] = array(
    'title' => t('List'),
    'access arguments' => array('access craigswatch'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -99,
  );
  $items['craigswatch/listings/delete'] = array(
    'title' => t('Delete all'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('craigswatch_listing_form'),
    'access arguments' => array('access craigswatch'),
    'type' => MENU_LOCAL_TASK,
    'weight' => -98,
  );

  $items['craigswatch/tutor'] = array(
    'title' => t('Tutor'),
    'page callback' => 'craigswatch_tutor',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
    'weight' => -97,
  );

  return $items;
}

/**
 * List view for watches.
 */
function craigswatch_watches() {
  global $user;
  $header = array(t('Country'), t('State'), t('City'), t('Heading'), t('Subheading'), t('Term 1'), t('Term 2'), t('Term 3'), t('Send email'), t('Operations'));
  $rows = array();
  $sql = sprintf("SELECT * FROM {craigswatch_watch} WHERE uid = %d ORDER BY id", $user->uid);
  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {
    $l_sql = sprintf("SELECT * FROM {craigswatch_location} WHERE id = %d", $data->lid);
    $l_result = db_query($l_sql);
    $l_data = db_fetch_object($l_result);
    $c_sql = sprintf("SELECT * FROM {craigswatch_category} WHERE id = %d", $data->cid);
    $c_result = db_query($c_sql);
    $c_data = db_fetch_object($c_result);
    $row = array($l_data->country, $l_data->state, $l_data->city, $c_data->heading, $c_data->subheading, $data->term_1, $data->term_2, $data->term_3, $data->send_email);
    $row[] = array('data' => l(t('edit'), 'craigswatch/watches/edit/'. $data->id .'/'. $data->lid .'/'. $data->cid .'/'));
    $rows[] = $row;
  }
  return theme('table', $header, $rows);
}

/**
 * Implementation of hook_form for watch.
 *
 * @ingroup forms
 * @see craigswatch_watch_form_validate()
 * @see craigswatch_watch_form_submit()
 */
function craigswatch_watch_form(&$form_state, $id = NULL, $lid = NULL, $cid = NULL) {
  $wdata = array();
  if ($id) {
    $wsql = sprintf("SELECT * FROM {craigswatch_watch} WHERE id = %d", $id);
    $wresult = db_query($wsql);
    $wdata = db_fetch_object($wresult);
  }
  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => $id,
  );
  $form['lid'] = array(
    '#type' => 'hidden',
    '#value' => $lid,
  );
  $form['cid'] = array(
    '#type' => 'hidden',
    '#value' => $cid,
  );
  $locations = array();
  $sql = "SELECT * FROM {craigswatch_location} ORDER BY id";
  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {
    $locations[] = $data->country .' '. $data->state .' '. $data->city;
  }
  $form['location'] = array(
    '#type' => 'select',
    '#title' => t('Location (country state city)'),
    '#options' => $locations,
    '#default_value' => $wdata->lid - 1,
    '#description' => t('The location for this watch.'),
    '#required' => TRUE,
  );
  $categories = array();
  $sql = "SELECT * FROM {craigswatch_category} ORDER BY id";
  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {
    $categories[] = $data->heading .' '. $data->subheading;
  }
  $form['category'] = array(
    '#type' => 'select',
    '#title' => t('Category (heading subheading)'),
    '#options' => $categories,
    '#default_value' => $wdata->cid - 1,
    '#description' => t('The category for this watch.'),
    '#required' => TRUE,
  );
  $form['term_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Term 1'),
    '#default_value' => $wdata->term_1,
    '#description' => t('The first (optional) search term for this watch.'),
  );
  $form['term_2'] = array(
    '#type' => 'textfield',
    '#title' => t('Term 2'),
    '#default_value' => $wdata->term_2,
    '#description' => t('The second (optional) search term for this watch.'),
  );
  $form['term_3'] = array(
    '#type' => 'textfield',
    '#title' => t('Term 3'),
    '#default_value' => $wdata->term_3,
    '#description' => t('The third (optional) search term for this watch.'),
  );
  $form['send_email'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send me an email when a new listing appears.'),
    '#default_value' => ($id) ? $wdata->send_email : 1,
    '#description' => t('Of course you can always check on this site for listings.'),
  );
  if ($id) {
    $form['update'] = array(
      '#type' => 'submit',
      '#value' => t('Update watch'),
      '#weight' => 45,
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete watch'),
      '#weight' => 50,
    );
  }
  else {
    $form['add'] = array(
      '#type' => 'submit',
      '#value' => t('Add watch'),
      '#weight' => 40,
    );
  }
  return $form;
}

/**
 * Implementation of hook_form_validate for watch.
 */
function craigswatch_watch_form_validate($form, &$form_state) {
  global $user;
  $ndx = $lid = 0;
  $lndx = $form_state['values']['location'];
  $sql = "SELECT * FROM {craigswatch_location} ORDER BY id";
  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {
    if ($ndx == $lndx) {
      $form_state['values']['lid'] = $data->id;
      break;
    }
    $ndx += 1;
  }
  $ndx = $cid = 0;
  $cndx = $form_state['values']['category'];
  $sql = "SELECT * FROM {craigswatch_category} ORDER BY id";
  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {
    if ($ndx == $cndx) {
      $form_state['values']['cid'] = $data->id;
      break;
    }
    $ndx += 1;
  }
  $lid = $form_state['values']['lid'];
  $cid = $form_state['values']['cid'];
  $term_1 = trim($form_state['values']['term_1']);
  $term_2 = trim($form_state['values']['term_2']);
  $term_3 = trim($form_state['values']['term_3']);
  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';
  if ($op == 'Delete watch') {
    $sql = sprintf("DELETE FROM {craigswatch_watch} WHERE `lid` = %d AND `cid` = %d AND `term_1` = '%s' AND `term_2` = '%s' AND `term_3` = '%s'", $lid, $cid, $term_1, $term_2, $term_3);
    $result = db_query($sql);
    if ($result) {
      drupal_set_message('Your watch entry has been deleted.');
    }
    drupal_goto('craigswatch/watches');
  }
  else if ($op == 'Add watch') {
    $sql = sprintf("SELECT * FROM {craigswatch_watch} WHERE `uid` = %d", $user->uid);
    $result = db_query($sql);
    while ($data = db_fetch_object($result)) {
      if ($lid == $data->lid) {
        if ($cid == $data->cid) {
          if (!strcmp($term_1, $data->term_1)) {
            if (!strcmp($term_2, $data->term_2)) {
              if (!strcmp($term_3, $data->term_3)) {
                form_set_error('watch', t('That watch already exists in the database.'));
              }
            }
          }
        }
      }
    }
  }
  if (!preg_match('!^[A-Za-z0-9]*$!', $term_1)) {
    form_set_error('term_1', t('The term_1 must contain only letters and numbers.'));
  }
  if (!preg_match('!^[A-Za-z0-9]*$!', $term_2)) {
    form_set_error('term_2', t('The term_2 must contain only letters and numbers.'));
  }
  if (!preg_match('!^[A-Za-z0-9]*$!', $term_3)) {
    form_set_error('term_3', t('The term_3 must contain only letters and numbers.'));
  }
}

/**
 * Implementation of hook_form_submit for watch.
 */
function craigswatch_watch_form_submit($form, &$form_state) {
  global $user;
  $id = $form_state['values']['id'];
  $lid = $form_state['values']['lid'];
  $cid = $form_state['values']['cid'];
  $term_1 = trim($form_state['values']['term_1']);
  $term_2 = trim($form_state['values']['term_2']);
  $term_3 = trim($form_state['values']['term_3']);
  $send_email = $form_state['values']['send_email'];
  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';
  if ($op == 'Add watch') {
    $sql = sprintf("INSERT INTO {craigswatch_watch} (`uid`, `lid`, `cid`, `timestamp`, `term_1`, `term_2`, `term_3`, `send_email`) VALUES (%d, %d, %d, %d, '%s', '%s', '%s', %d)", $user->uid, $lid, $cid, time(), $term_1, $term_2, $term_3, $send_email);
    $result = db_query($sql);
    if ($result) {
      drupal_set_message('Your watch entry has been added.');
    }
  }
  else if ($op == 'Update watch') {
    $sql = sprintf("UPDATE {craigswatch_watch} SET `lid` = %d, `cid` = %d, `term_1` = '%s', `term_2` = '%s', `term_3` = '%s', `send_email` = %d WHERE id = %d", $lid, $cid, $term_1, $term_2, $term_3, $send_email, $id);
    $result = db_query($sql);
    if ($result) {
      drupal_set_message('Your watch entry has been updated.');
    }
  }
  drupal_goto('craigswatch/watches');
}

/**
 * List view for listings.
 */
function craigswatch_listings() {
  global $user;
  $header = array(t('Listing'));
  $rows = array();
  $sql = sprintf("SELECT * FROM {craigswatch_listing} WHERE uid = %d ORDER BY id DESC", $user->uid);
  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {
    $listing = str_replace('|', '/', $data->listing);
    $rows[] = array($listing);
  }
  return theme('table', $header, $rows);
}

/**
 * Implementation of hook_form for listing.
 */
function craigswatch_listing_form(&$form_state) {
  global $user;
  $sql = sprintf("DELETE FROM {craigswatch_listing} WHERE `uid` = %d", $user->uid);
  $result = db_query($sql);
  drupal_set_message('Your listings have been deleted.');
  drupal_goto('craigswatch/listings');
}

/**
 * Tutor view for listings.
 */
function craigswatch_tutor() {
  $items = array(
    t('Click Craigswatch > Watches.'),
    t('Click Watches > Add.'),
    t('Select a Location and a Category from the drop down lists.'),
    t('Enter from one to three search terms.'),
    t('Click the "Add watch" button.'),
    t('Wait for at least an hour, then check your "Listings" by clicking Craigswatch > Listings.'),
    t('Whenever a new listing appears matching your criteria, an email will be sent to you.'),
  );
  return theme('item_list', $items);
}

